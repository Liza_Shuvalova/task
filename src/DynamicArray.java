import java.util.Arrays;

public class DynamicArray<Obj> {
    private Obj[] elements;
    private int numberOfElements;

    public DynamicArray(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + index);
        }
        elements = (Obj[]) new Object[index];
    }

    public DynamicArray() {
        this(10);
    }

    private void checkIndex(int index) {
        if (index < 0 || index > numberOfElements) {
            throw new IndexOutOfBoundsException("Index " + index + " is out of bounds. Size: " + (numberOfElements + 1));
        }
    }

    public boolean add(Obj e) {
        try {
            elements[numberOfElements] = e;
        } catch (ArrayIndexOutOfBoundsException ex) {
            increaseArray();
            elements[numberOfElements] = e;
        }
        numberOfElements++;
        return true;
    }

    private void increaseArray() {
        int newSize = (int) (elements.length * 1.5);
        elements = Arrays.copyOf(elements, newSize);
    }

    public void add(int index, Obj e) {
        checkIndex(index);
        try {
            System.arraycopy(elements, index, elements, index + 1, numberOfElements - index);
        } catch (ArrayIndexOutOfBoundsException ex) {
            increaseArray();
            System.arraycopy(elements, index, elements, index + 1, numberOfElements - index);
        }
        elements[index] = e;
        numberOfElements++;
    }

    public void set(int index, Obj e) {
        checkIndex(index);
        elements[index] = e;
    }

    public Obj get(int index) {
        checkIndex(index);
        return elements[index];
    }

    public Obj remove(int index) {
        checkIndex(index);
        Obj result = elements[index];
        if (index < numberOfElements) {
            System.arraycopy(elements, index + 1, elements, index, elements.length - 1 - index);
        }
        elements[elements.length - 1] = null;
        numberOfElements--;
        return result;
    }

    public boolean remove(Obj e) {
        for (int i = 0; i < numberOfElements; i++) {
            if (elements[i].equals(e)) {
                remove(i);
                return true;
            }
        }
        return false;
    }

    public int size() {
        return numberOfElements;
    }

    public int indexOf(Obj e) {
        int i = 0;
        for (; i < numberOfElements; i++) {
            if (elements[i].equals(e)) {
                break;
            }
        }
        return i;
    }

    public boolean contains(Obj e) {
        for (int i = 0; i < numberOfElements; i++) {
            if (elements[i].equals(e)) {
                return true;
            }
        }
        return false;
    }

    public Obj[] toArray() {
        Obj[] array = (Obj[]) new Object[numberOfElements];
        for (int i = 0; i <= numberOfElements; i++) {
            array[i] = elements[i];
        }
        return array;
    }
}