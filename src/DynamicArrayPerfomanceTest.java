import java.util.ArrayList;

public class DynamicArrayPerfomanceTest {
    public static void main(String[] args) {
        testAddAndRemoveElementsInBeggining();
        testAddAndRemoveElementInMiddle();
        testAddAndRemoveElementInEnd();
    }

    public static void testAddAndRemoveElementsInBeggining() {
        DynamicArray<Number> testDynamicArray = new DynamicArray<>(1000000);
        for (int i = 0; i < 1000000; i++) {
            testDynamicArray.add(i);
        }
        ArrayList<Number> testArrayList = new ArrayList<>(1000000);
        for (int i = 0; i < 1000000; i++) {
            testArrayList.add(i);
        }
        StopWatch timer = new StopWatch();
        System.out.println("Addition/remove to/from the beggining test");
        timer.start();
        testDynamicArray.add(0, 45);
        System.out.println("DynamicArray.add(e): " + timer.getElapsedTime());
        timer.start();
        testArrayList.add(0, 45);
        System.out.println("ArrayList.add(e): " + timer.getElapsedTime());
        timer.start();
        testDynamicArray.remove(0);
        System.out.println("DynamicArray.remove(e): " + timer.getElapsedTime());
        timer.start();
        testArrayList.remove(0);
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime());
    }

    public static void testAddAndRemoveElementInMiddle() {
        DynamicArray<Number> testDynamicArray = new DynamicArray<>(1000000);
        for (int i = 0; i < 1000000; i++) {
            testDynamicArray.add(i);
        }
        ArrayList<Number> testArrayList = new ArrayList<>(1000000);
        for (int i = 0; i < 1000000; i++) {
            testArrayList.add(i);
        }
        StopWatch timer = new StopWatch();
        System.out.println("Addition/remove to/from the middle test");
        timer.start();
        testDynamicArray.add(500000, 45);
        System.out.println("DynamicArray.add(e): " + timer.getElapsedTime());
        timer.start();
        testArrayList.add(500000, 45);
        System.out.println("ArrayList.add(e): " + timer.getElapsedTime());
        timer.start();
        testDynamicArray.remove(500000);
        System.out.println("DynamicArray.remove(e): " + timer.getElapsedTime());
        timer.start();
        testArrayList.remove(500000);
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime());
    }

    public static void testAddAndRemoveElementInEnd() {
        DynamicArray<Number> testDynamicArray = new DynamicArray<>(1000000);
        for (int i = 0; i < 1000000; i++) {
            testDynamicArray.add(i);
        }
        ArrayList<Number> testArrayList = new ArrayList<>(1000000);
        for (int i = 0; i < 1000000; i++) {
            testArrayList.add(i);
        }
        StopWatch timer = new StopWatch();
        System.out.println("Addition/remove to/from the end test");
        timer.start();
        testDynamicArray.add(testDynamicArray.size() - 1, 222);
        System.out.println("DynamicArray.add(e): " + timer.getElapsedTime());
        timer.start();
        testArrayList.add(testArrayList.size() - 1, 222);
        System.out.println("ArrayList.add(e): " + timer.getElapsedTime());
        timer.start();
        testDynamicArray.remove(testDynamicArray.size() - 1);
        System.out.println("DynamicArray.remove(e): " + timer.getElapsedTime());
        timer.start();
        testArrayList.remove(testArrayList.size() - 1);
        System.out.println("ArrayList.remove(e): " + timer.getElapsedTime());
    }
}

class StopWatch {
    public long startTime;
    public long finishTime;

    public long start() {
        startTime = System.currentTimeMillis();
        return startTime;
    }

    public long getElapsedTime() {
        finishTime = System.currentTimeMillis();
        return finishTime - startTime;
    }
}
