public class DynamicArrayTest {
    public static void main(String[] args) {
        testAddElementInBeggining();
        testAddElementInMiddle();
        testAddElementInEnd();
        testRemoveElementInBeggining();
        testRemoveElementInMiddle();
        testRemoveElementInEnd();
    }

    private static void testAddElementInBeggining() {
        DynamicArray<Number> testDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            testDynamicArray.add(i);
        }
        testDynamicArray.add(0, 17);
        assertEquals("Lesson03DynamicArray.testAddElementInBeggining", 17, testDynamicArray.get(0));
    }

    private static void testAddElementInMiddle() {
        DynamicArray<Number> testDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            testDynamicArray.add(i);
        }
        testDynamicArray.add(((testDynamicArray.size() - 1) / 2), 20);
        assertEquals("Lesson03DynamicArray.testAddElementInMiddle", 20, testDynamicArray.get((testDynamicArray.size() - 1) / 2));
    }

    private static void testAddElementInEnd() {
        DynamicArray<Number> testDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            testDynamicArray.add(i);
        }
        testDynamicArray.add(testDynamicArray.size(), 23);
        assertEquals("Lesson03DynamicArray.testAddElementInEnd", 23, testDynamicArray.get(testDynamicArray.size() - 1));
    }

    private static void testRemoveElementInBeggining() {
        DynamicArray<Number> testDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            testDynamicArray.add(i);
        }
        testDynamicArray.remove(0);
        assertEquals("Lesson03DynamicArray.testRemoveElementInBeggining", 1, testDynamicArray.get(0));
    }

    private static void testRemoveElementInMiddle() {
        DynamicArray<Number> testDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            testDynamicArray.add(i);
        }
        testDynamicArray.remove(testDynamicArray.size() / 2);
        assertEquals("Lesson03DynamicArray.testRemoveElementInMiddle", 8, testDynamicArray.get(testDynamicArray.size() / 2));
        System.out.println();
    }

    private static void testRemoveElementInEnd() {
        DynamicArray<Number> testDynamicArray = new DynamicArray<>(15);
        for (int i = 0; i < 15; i++) {
            testDynamicArray.add(i);
        }
        testDynamicArray.remove(testDynamicArray.size() - 1);
        assertEquals("Lesson03DynamicArray.testRemoveElementInEnd", null, testDynamicArray.get(testDynamicArray.size() - 1)); //failed
    }

    public static void assertEquals(String testName, Number expected, Number actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }
}